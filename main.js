'use strict';

// Modules to control application life and create native browser window
const {app, BrowserWindow, Menu} = require('electron');
const electron = require('electron');
const {ipcMain} = require('electron');

const path = require('path');
const url = require('url');



// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow() {
    // const {width, height} = require('electron').screen.getPrimaryDisplay().size;

    // Create the browser window.
    mainWindow = new BrowserWindow({width: electron.screen.getPrimaryDisplay().size.width, height: electron.screen.getPrimaryDisplay().size.height});

    // and load the index.html of the app.
    // mainWindow.loadFile('index.html')

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'public', 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    const template = [
        {
            label: 'File',
            submenu: [
                {
                    label: 'Settings',
                    click() {
                        let settingsWindow = new BrowserWindow({width: 500, height: 500});
                        settingsWindow.loadURL(url.format({
                            pathname: path.join(__dirname, 'public', 'settings.html'),
                            protocol: 'file:',
                            slashes: true
                        }));

                        // settingsWindow.webContents.openDevTools();
                    }
                }
            ]
        }
    ];

    // TODO Hack 5000!
    ipcMain.on("updateImage", function (event, args) {

        if (args === "../images/deadmau5") {
            console.log("ajsdkgjk");
            mainWindow.webContents.executeJavaScript(
                "var image = document.getElementById(\"image\");image.innerHTML = \"<div class=\\\"logo-container\\\"><div class=\\\"ear-container\\\">\<div class=\\\"ear left\\\"></div><div class=\\\"space\\\"></div><div class=\\\"ear right\\\"></div></div><div class=\\\"face-container\\\"><div class=\\\"face\\\"><div class=\\\"eyes-container\\\"><div class=\\\"eye left\\\"></div><div class=\\\"eye-space\\\"></div><div class=\\\"eye right\\\"></div></div><div class=\\\"mouth-container\\\"><div class=\\\"mouth left\\\"></div><div class=\\\"mouth right\\\"></div></div></div></div></div>\";");
        } else {
            mainWindow.webContents.executeJavaScript(
                "var image = document.getElementById(\"image\");" +
                "image.innerHTML = \"\";" +
                "var imagex = document.createElement(\"img\");" +
                "imagex.src = \"" + args + "\";" +
                "image.appendChild(imagex);"
            );
        }
    });


    mainWindow.setMenu(Menu.buildFromTemplate(template));


    // Open the DevTools.
    // mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);


// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
