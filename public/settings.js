window.onload = function () {
    const fs = require('fs');
    const { ipcRenderer } = require('electron');


    let imageUpload = document.getElementById("imageUpload");
    let addFolder = document.getElementById("addFolder");
    let imageApplyButton = document.getElementById("applyImage");
    let musicFolderApplyButton = document.getElementById("applyMusic");
    let imageFilePath = "";
    let musicPath = "";
    let soundPaths = [];
    let imagesArr = ["deadmau5"];
    let actualImage = "";

    // let mainDocument =

    let mediaPaths = document.getElementById("mediaPaths");
    let images = document.getElementById("images");

    let settingsFilePath = "settings/settings.xml";
    let imageFolderPath = "images/";

    loadImagesSettings()
        .then(function () {
            displayArray(imagesArr, images);
        });

    loadMusicSettings()
        .then(function () {
            displayArray(soundPaths, mediaPaths);
        });

    imageUpload.addEventListener("input", function () {
        imageFilePath = this.files[0].path;
        console.log(imageFilePath);
    });

    addFolder.addEventListener("input", function () {
        musicPath = this.files[0].path;
    });

    imageApplyButton.addEventListener("click", function () {
        let fileName = imageFilePath.substring(imageFilePath.lastIndexOf('/') + 1);
        fs.createReadStream(imageFilePath).pipe(fs.createWriteStream(imageFolderPath + fileName));
        reload();
    });

    musicFolderApplyButton.addEventListener("click", function () {

        // let fileName = imageFilePath.substring(imageFilePath.lastIndexOf('/')+1);
        // fs.createReadStream(imageFilePath).pipe(fs.createWriteStream(imageFolderPath + fileName));
        reload();
    });

    function updateImageSettings(imageName) {
        fs.readFile(settingsFilePath, 'utf8', function (err, data) {
            if (!err) {
                console.log(data);
            }
            actualImage = data.match("<image>(.*?)</image>")[1];
            data = data.replace(actualImage, imageName);
            fs.writeFile(settingsFilePath, data, function (err) {
                if (!err) {
                    console.log("saved!");
                }
            });
        });
    }

    function loadImagesSettings() {
        return new Promise(function (resolve) {
                fs.readdir(imageFolderPath, function (err, data) {
                    for (var i = 0; i < data.length; i++) {
                        console.log("asdfasdf", data[i]);
                        imagesArr.push(data[i]);
                    }
                    resolve(imagesArr);
                });
            }
        )
    }

    function loadMusicSettings() {
        return new Promise(function (resolve) {
                fs.readFile(settingsFilePath, 'utf8', function (err, data) {
                    if (!err) {
                        console.log(data);
                    }
                    actualImage = data.match("<image>(.*?)</image>")[1];
                    let regex = new RegExp("<folderPath>(.*?)</folderPath>", "g");
                    let match;
                    while (match = regex.exec(data)) {
                        soundPaths.push(match[1]);
                        regex.lastIndex = match.index + 1;
                    }
                    resolve(soundPaths);
                });
            }
        )
    }

    function displayArray(arr, container) {
        for (var i = 0; i < arr.length; i++) {
            let imageName = arr[i];
            let el = document.createElement("div");
            let text = document.createTextNode(imageName);
            el.appendChild(text);
            container.appendChild(el);
            el.addEventListener("click", function () {
                updateImageSettings(this.innerHTML);
                ipcRenderer.send("updateImage","../" + imageFolderPath + this.innerHTML);
            })
        }
    }

    function reload() {
        location.reload();
    }
};