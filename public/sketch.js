'use strict';
var soundFile;
var play, image;
var size, style, initSize;
var filter, fft;
var bpm, bpmCounter, bpmForward, refreshRate, framerate;
var soundList;

function preload() {
    soundFormats('mp3', 'ogg');
    soundFile = loadSound('../media/deadmau5');
}

function init(){
    framerate = 60;
    frameRate(framerate);
    soundList = getFilesForSoundList();
    bpmCounter = 0;
    bpmForward = true;

    noLoop();
    image = document.getElementById("image");
    play = document.getElementById("play");
    play.addEventListener("click", function () {
        play.classList.toggle("paused");
        if (!play.classList.contains("paused")) {
            soundFile.pause();
            noLoop();
        } else {
            soundFile.play();
            loop();
        }
    });
}

async function setup() {
    init();
    //TODO: try getting bpm on an other way
    bpm = 92;
    refreshRate = framerate / ((bpm*2)/framerate);
    var canvas = createCanvas(window.innerWidth - 15, 200);
    canvas.parent('soundSpectrum');
    fill(0, 0, 10);

    fft = new p5.FFT();

    style = window.getComputedStyle(image);
    size = style.getPropertyValue("width");
    initSize = size;

    actualizeSoundList();


    await new Promise(resolve => setTimeout(resolve, 2000));
}

function draw() {

    drawSpectrum();
    drawImage();
}


function drawSpectrum() {
    background('#888888');
    var spectrum = fft.analyze();
    for (var i = 0; i < spectrum.length; i += 10) {
        var x = map(i, 0, 740, 0, width / 10) * 10;
        var h = -height + map(spectrum[i], 0, 255, height, 0);
        rect(x, height, width / spectrum.length * 10, h);
    }
}

function drawImage() {
    if (bpmForward) {
        bpmCounter++;
        if (bpmCounter >= refreshRate) {
            bpmForward = false;
        }
    } else {
        bpmCounter--;
        if (bpmCounter <= 0) {
            bpmForward = true;
        }
    }
    image.setAttribute("style", "width:" + (parseInt(initSize) + (bpmCounter * 3)) + ";height:" + (parseInt(initSize) + (bpmCounter * 3)) + ";");
}


function actualizeSoundList(){
    let musicList = document.getElementById("musicList");
    for(let i = 0; i<soundList.length; i++){
        let element = document.createElement("li");
        let textNode = document.createTextNode(soundList[i]);
        element.appendChild(textNode);
        element.addEventListener("click",async function(){
            soundFile.stop();
            noLoop();
            soundFile =loadSound('../media/' + soundList[i]);
            play.classList.remove("paused");
            await new Promise(resolve => setTimeout(resolve, 1000));
            soundFile.play();
            play.classList.add("paused");
            loop();

        });
        musicList.appendChild(element);
    }
}

function getFilesForSoundList(){
    const fs= require('fs');
    const path = require('path');
    return fs.readdirSync(path.join(__dirname,'..', 'media'));
}